#!/bin/bash

for d in `ls -d ./$1*`; do
  cat $d/events.xml | grep -E "xml|events|arrival" > $d/arrivals.xml
done
